$(function () {

    var HOST_URL = 'https://my.yeti.onlinepatent.ru';

    /* sidemenu */

    $('.sandwich, .cover, .side-menu-close').on('click', function () {
        $('.side-menu').toggleClass('is-on');
        $('.cover').toggleClass('is-on');
    });

    $('.side-menu .navigation button').on('click', function () {
        $(this).parent('.with-dropdown').toggleClass('is-on').siblings().removeClass('is-on')
    });

    /* validation */

    $('.form-control').each(function () {
        $(this).addClass('ng-untouched').val('')
    }).on('blur change', function () {
        validateInput(this)
    });

    function validateInput(element) {
        var el = $(element),
            parent = el.parents('.has-validator'),
            spinner = $('.spinner'),
            val = el.val(),
            hasError = true;

        el.addClass('ng-touched').removeClass('ng-untouched');

        if ((el.attr('type') === 'email') && !validateEmail(val) && (val !== '')) {
            parent.siblings('.error-format').show().siblings('.error').hide();
            el.addClass('ng-invalid');
        }
        else if ((el.attr('type') === 'checkbox') && !el.is(':checked')) {
            parent.siblings('.error').show();
            el.addClass('ng-invalid')
        }
        else if (parent.hasClass('is-required') && (val === '')) {
            parent.siblings('.error-required').show().siblings('.error').hide();
            el.addClass('ng-invalid')
        }
        else if ((el.attr('name') === 'username')) {
            $.ajax({
                type: "GET",
                url: HOST_URL + "/client-api/auth/checkUsernameAvailability?username=" + el.val(),
                dataType: "application/json",
                accepts: {
                    text: "application/json"
                },
                // async: false,
                beforeSend: function () {
                    spinner.addClass('is-on');
                },
                complete: function (data) {
                    $('.spinner').removeClass('is-on');
                    var response = JSON.parse(data.responseText).isAvailable;

                    if (response) {
                        parent.siblings('.error-exist').show().siblings('.error').hide();
                        el.addClass('ng-invalid')
                    }
                    else {
                        el.removeClass('ng-invalid').addClass('ng-valid');
                        parent.siblings('.error').hide();
                        spinner.removeClass('is-on');
                        hasError = false
                    }
                }
            })
        }
        else {
            el.removeClass('ng-invalid').addClass('ng-valid');
            parent.siblings('.error').hide();
            hasError = false
        }


        return hasError;
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    /* submit form */

    $('#registration').on('submit', function () {

        var formBlock = $(this).parent(),
            formValid = true;

        $(this).find('.form-control').each(function () {
            validateInput(this)
            formValid = $(this).hasClass('ng-invalid') ? false : formValid;
        });

        if (formValid) {

            formBlock.addClass('busy');

            var username = $(this).find("input[name=username]").val(),
                lastName = $(this).find("input[name=lastName]").val(),
                firstName = $(this).find("input[name=firstName]").val(),
                phone = $(this).find("input[name=phone]").val();

            $.ajax({
                type: "POST",
                url: HOST_URL + "/client-api/auth/register",
                data: {
                    "username": username,
                    "lastName": lastName,
                    "firstName": firstName,
                    "phone": phone,
                    "hasAgreedWithTerms": true
                },
                dataType: "application/json",
                accepts: {
                    text: "application/json"
                },
                beforeSend: function () {
                    formBlock.addClass('busy');
                    $('.error').hide()
                },
                complete: function (data) {
                    formBlock.removeClass('busy');
                    var response = JSON.parse(data.responseText);
                    console.info(response);

                    if (response.status === 'ok') {
                        //todo: redirect to ...
                    }
                    else {
                        $('.error-ajax').html(response.message).show()
                    }
                }
            })
        }

        return false
    });

    $('#login').on('submit', function () {

        var formBlock = $(this).parent(),
            formValid = true;

        $(this).find('.form-control').each(function () {
            validateInput(this)
            formValid = $(this).hasClass('ng-invalid') ? false : formValid;
        });

        if (formValid) {


            var email = $(this).find("input[name=email]").val(),
                password = $(this).find("input[name=password]").val(),
                captcha = $(this).find("input[name=captcha]").val();

            $.ajax({
                type: "POST",
                url: HOST_URL + "/api/infrastructure/validateuser",
                data: {
                    "email": email,
                    "password": password,
                    "captchaResponse": captcha
                },
                dataType: "application/json",
                accepts: {
                    text: "application/json"
                },
                beforeSend: function () {
                    formBlock.addClass('busy');
                    $('.error').hide()
                },
                complete: function (data) {
                    formBlock.removeClass('busy');
                    var response = JSON.parse(data.responseText);
                    console.info(response);

                    if (response.status === 'ok') {
                        //todo: redirect to ...
                    }
                    else {
                        $('.error-ajax').html(response.message).show();
                        if (response.willNeedCaptcha) {
                            $('.form-group-captcha').show();
                        }
                        else {
                            $('.form-group-captcha').hide();
                        }
                    }
                }
            })
        }

        return false
    });

});